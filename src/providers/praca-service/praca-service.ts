import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFireDatabase,
  FirebaseListObservable,
  FirebaseObjectObservable
} from 'angularfire2/database-deprecated';
import * as firebase from 'firebase/app';
import { Ipozycje } from '../../models/pozycje';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
export class Pozs {
  constructor(public wll: number, public auf: string) {}
}
@Injectable()
export class PracaServiceProvider {
  user: firebase.User;
  authState: Observable<firebase.User>;
  naddatek: any;
  maszyna: any;
  keyS: any;
  currentUser: any;
  wll: any;
  l1: number;
  m: any;
  nici: any;
  auf: any;
  ilosc: any;
  czas: any;
  listaPozycji: FirebaseListObservable<Ipozycje[]>;
  photos: FirebaseListObservable<any[]>;
  data: any;
  pozycje: FirebaseListObservable<any>;
  naddatki: FirebaseListObservable<any>;
  pozycjaDetail: FirebaseObjectObservable<any>;
  pozycjaId: any;
  id: any;
  p: any;
  pf: any;
  date: any;
  uid: any;
  userId: any;
  auth: any;
  mojNaddatek: any;
  naddatekDetail: any;
  idPozycji: any;
  idN: any;
  itemsN: any;
  naddatekP: any;
  dataN: any;
  formProd: FirebaseListObservable<any>;
  formProd2: FirebaseObjectObservable<any>;
  formProd3: FirebaseListObservable<any[]>;
  fireTimestamp: any;
  robota: Array<any>;
  robot;
  pozycjeArr: any;
  endT: number;
  wartoscArr = [];
  wartoscArr2 = [];
  viceroy = firebase.database().ref('/listaPozycji');
  note;
  value: any;
  // last24: any;
  lastWeek: any;
  dateNow = Date.now();
  lastDay = Date.now() - 86400000;
  pozs: Pozs[] = [];
  lists;
  private sumSztuk = 0;
  private sum = new Subject<number>();
  constructor(
    private afAuth: AngularFireAuth,
    public afd: AngularFireDatabase,
    public http: Http
  ) {
    console.log('Hello Praca Provider');
    this.authState = this.afAuth.authState;
  }

  savePhoto(imageData) {
    return this.afd.list('/photos').push({
      imageData: imageData,
      dateNow: this.dateNow,
      title: 'tytul' + this.dateNow
    });
  }

  getPhotos() {
    return this.afd.list('/photos').map(lst => lst.reverse());
  }

  pozycjeRef() {
    return this.afd.list('/listaPozycji');
  }

  deletePhoto(key) {
    return this.afd.list('/photos/' + key).remove();
  }

  archivePhoto(photo) {
    let photoTemp = photo.imageData;

    return this.afd.list('/photos/').update(photo.$key, {
      imageData: '',
      imageArch: photoTemp
    });
  }

  promisePozycje(hours) {
    let result: number = Date.now() - hours * 3600000;
    let promise = new Promise((resolve, reject) => {
      let items = this.afd
        .list('/listaPozycji', {
          query: {
            orderByChild: 'fireTimestamp',
            startAt: result
          }
        })
        .map(array => array.reverse());
      resolve(items);
    });
    return promise;
  }

  getTimes(wll, l1) {
    let promise = new Promise((resolve, reject) => {
      let times = this.afd
        .list('/listaPozycji', {
          query: {
            orderByChild: 'fireTimestamp'
          }
        })
        .map(array => array.reverse())
        .map(poKazdym =>
          poKazdym
            .filter(fi => fi.wll == wll)
            .filter(filt => filt.l1 == l1)
            .filter(ftr => ftr.czas != '')
        );
      resolve(times);
    });
    return promise;
  }

  getLastAufS() {
    let promise = new Promise((resolve, reject) => {
      let time: number = Date.now() - 48 * 3600000;
      let last = this.afd
        .list('/listaPozycji', {
          query: {
            orderByChild: 'fireTimestamp',
            startAt: time
          }
        })
        .map(array => array.reverse());
      resolve(last);
    });
    return promise;
  }

  wyswietlPozycje(): FirebaseListObservable<any> {
    return this.afd
      .list('/listaPozycji', {
        query: {
          orderByChild: 'fireTimestamp'
        }
      })
      .map(array => array.reverse()) as FirebaseListObservable<any[]>;
  }

  viewPozycjeAll(): FirebaseListObservable<any> {
    return this.afd
      .list('/listaPozycji', {
        query: {
          orderByChild: 'fireTimestamp'
        }
      })
      .map(array => array.reverse()) as FirebaseListObservable<any[]>;
  }

  viewPozycjeWeek(): FirebaseListObservable<any> {
    let lastWeekPosition = this.afd
      .list('/listaPozycji', {
        query: {
          orderByChild: 'fireTimestamp',
          startAt: this.lastWeek
        }
      })
      .map(array => array.reverse()) as FirebaseListObservable<any[]>;
    return lastWeekPosition;
  }

  usunPozycje(id) {
    return this.afd.list('/listaPozycji').remove(id);
  }
  wyswietlNaddatki(): FirebaseListObservable<any> {
    return this.afd.list('/listaNaddatkow');
  }

  getNaddatki() {
    return this.naddatki;
  }

  savePozycja(data) {
    return this.afd.list('/listaPozycji').push({
      wll: data.wll,
      l1: data.l1,
      m: data.m,
      nici: data.nici,
      auf: data.auf,
      aufFormat: data.aufFormat,
      ilosc: data.ilosc,
      czas: data.czas,
      note: '',
      fireTimestamp: firebase.database.ServerValue.TIMESTAMP,
      dateNow: this.dateNow
    });
  }

  savePozycjaAdd(data): any {
    let promise1 = new Promise((resolve, reject) => {
      this.getWagi().forEach(arr1 => {
        arr1.forEach(element => {
          if (element.wll === data.wll && element.l1 === data.l1) {
            let elWaga = element.waga;
            resolve(elWaga);
          }
        }); //foreach
      }); //get wagi
    }); //promise new

    var promise2 = new Promise((resolve, reject) => {
      setTimeout(function() {
        resolve(0);
      }, 1000);
    });

    Promise.race([promise1, promise2]).then(wygral => {
      let email = this.afAuth.auth.currentUser.email;
      let authorId = this.afAuth.auth.currentUser.uid;
      let licz = this.setLicznik(data);

      let aufFormat = this.aufFormat(data);

      if (wygral > 0) {
        this.afd.list('/listaPozycji').push({
          wll: data.wll,
          l1: data.l1,
          auf: data.auf,
          aufFormat: aufFormat,
          m: licz,
          nici: data.nici,
          ilosc: data.szt,
          czas: data.czas,
          note: '',
          fireTimestamp: firebase.database.ServerValue.TIMESTAMP,
          dateNow: this.dateNow,
          authorEmail: email,
          authorId: authorId,
          waga: wygral
        });
      } else {
        this.afd.list('/listaPozycji').push({
          wll: data.wll,
          l1: data.l1,
          auf: data.auf,
          aufFormat: aufFormat,
          m: licz,
          nici: data.nici,
          ilosc: data.szt,
          czas: data.czas,
          note: '',
          fireTimestamp: firebase.database.ServerValue.TIMESTAMP,
          dateNow: this.dateNow,
          authorEmail: email,
          authorId: authorId
        });
      }
    });
  } //savepoz

  setLicznik(data) {
    let wll = data.wll;
    let l1 = data.l1;
    // od 1t do 4
    if (wll >= 1 && wll <= 4) {
      let licz = l1 * 1 * 22;
      return licz;
    } else {
    }
    // od 5t  do 8
    if (wll >= 5 && wll <= 8) {
      let licz = l1 * 1 * 20;
      return licz;
    } else {
      let licz = 0;
      return licz;
    }
  }

  // Format zlecenia -------------------------------------------------------------------
  aufFormat(data) {
    if (data.auf.length > 8) {
      // if (data.auf.length < 8) {
      //   if (data.auf.length > 9) {
      let auf0 = data.auf.substr(0, 3); //777888;
      let auf1 = data.auf.substr(3, 3); // 777888;
      let auf2 = data.auf.substr(6, 2); // 01
      let auf3 = data.auf.substr(8, 2); // pozycja
      let aufFormat = auf0 + ' ' + auf1 + '/' + auf2 + '/' + auf3;
      return aufFormat;
      // }
    } else {
      return data.auf;
      // }
    }
  }
  dodajWage(data, waga) {
    return this.afd.object('/listaWag/' + data.$key).update({
      wll: data.wll,
      l1: data.l1,
      waga: waga,
      date: firebase.database.ServerValue.TIMESTAMP
    });
  }
  dodajWageDoPoz(data, waga) {
    return this.afd.object('/listaPozycji/' + data.$key).update({
      waga: waga,
      updateDate: firebase.database.ServerValue.TIMESTAMP
    });
  }

  addNoteServ(id, note) {
    return this.afd.object('/listaPozycji/' + id).update({
      notatka: note
    });
  }

  updatePozycja(id, data) {
    return this.afd.object('/listaPozycji/' + id).update({
      wll: data.wll,
      l1: data.l1,
      m: data.m,
      nici: data.nici,
      auf: data.auf,
      ilosc: data.ilosc,
      czas: data.czas,
      updateDate: firebase.database.ServerValue.TIMESTAMP,
      dateNow: this.dateNow
    });
  }

  zapiszNaddatek(data) {
    return this.afd.list('/listaNaddatkow').push({
      wll: data.wll,
      l1: data.l1,
      maszyna: data.maszyna,
      mojNaddatek: data.mojNaddatek,
      updateDate: firebase.database.ServerValue.TIMESTAMP,
      dateNow: this.dateNow
    });
  }

  updateNaddatek(id, data) {
    return this.afd.list('/listaNaddatkow/').update(id, {
      wll: data.wll,
      l1: data.l1,
      maszyna: data.maszyna,
      mojNaddatek: data.mojNaddatek
    });
  }

  removeNaddatek(naddatekId) {
    return this.afd.list('/listaNaddatkow/').remove(naddatekId);
  }

  getWagi() {
    return this.afd.list('/listaWag');
  }

  addSztuk(sztuk) {
    this.sumSztuk += sztuk;
    this.sum.next(this.sumSztuk);
  }

  getSumSztuk(): Observable<number> {
    return this.sum.asObservable();
  }
}
