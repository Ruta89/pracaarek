import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Observable } from 'rxjs/Observable';
import { PracaServiceProvider } from './../providers/praca-service/praca-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: Array<{ title: string; component: any }>;
  user: Observable<{
    email: string;
    password: string;
  }>;
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    pracaService: PracaServiceProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      pracaService.authState.subscribe(user => {
        if (user) {
          this.rootPage = 'TabsPage';
        } else {
          this.rootPage = 'LoginPage';
        }
      });

    this.pages = [
      { title: 'Pozycje', component: 'ViewPage' },
      { title: 'Dodaj', component: 'AddPage' },
      { title: 'Praca', component: 'PracaPage' },
      { title: 'Waga', component: 'WagaPage' },
      { title: 'Naddatki', component: 'NaddatkiPage' },
      { title: 'Czas', component: 'ViewTimePage' },
      { title: 'Timer', component: 'TimerPage' },
      { title: 'Zdjecia', component: 'TakePhotoPage' },
      { title: 'Tab', component: 'TabsPage' }
    ];
  });
  }
  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
