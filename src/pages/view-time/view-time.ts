import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PracaServiceProvider } from '../../providers/praca-service/praca-service';

@IonicPage()
@Component({
  selector: 'page-view-time',
  templateUrl: 'view-time.html'
})
export class ViewTimePage {
  times2;
  tonaz: number;
  dlugosc: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private pracaService: PracaServiceProvider
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewTimePage');
  }

  getTime() {
    if (this.tonaz > 0) {
      if (this.dlugosc > 0) {
        return this.pracaService
          .getTimes(this.tonaz, this.dlugosc)
          .then(dane => {
            this.times2 = dane;
          })
          .catch(err => {
            alert(err);
          });
      }
    }
  }

  openDetail(data) {
    this.navCtrl.push('DetailPozycjaPage', {
      pozycja: data
    });
  }
}
