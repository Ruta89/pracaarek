import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewTimePage } from './view-time';
import { MomentModule } from 'angular2-moment';
@NgModule({
  declarations: [
    ViewTimePage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(ViewTimePage),
  ],
})
export class ViewTimePageModule {}
