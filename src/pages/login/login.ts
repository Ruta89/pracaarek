import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string = '1@1.pl';
  password: string = '123456';
  user: any = {};
  authState:any;
  constructor(private afAuth: AngularFireAuth, public authService: AuthServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.authState = afAuth.authState;


  }

  login(email, password){
    this.authService.emailLogin(email, password);
  }

  register() {
    this.navCtrl.push('RegisterPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
