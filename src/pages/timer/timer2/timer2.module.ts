import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Timer2Page } from './timer2';

@NgModule({
  declarations: [
    Timer2Page,
  ],
  imports: [
    IonicPageModule.forChild(Timer2Page),
  ],
})
export class Timer2PageModule {}
