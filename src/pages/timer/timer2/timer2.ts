import { Component, OnInit, OnDestroy } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { TimerObservable } from "rxjs/observable/TimerObservable";

@IonicPage()
@Component({
  selector: "page-timer2",
  templateUrl: "timer2.html"
})
export class Timer2Page implements OnInit, OnDestroy {
  ticks1 = 0;

   ticks2;
  private subscription: Subscription;

  ticks3 = 0;
  private timer3;
  private sub: Subscription;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad Timer2Page");
  }

  ngOnInit() {
    let timer1 = Observable.timer(2000, 1000);
    timer1.subscribe(t => (this.ticks1 = t));

    let timer2 = TimerObservable.create(2000, 1000);
    this.subscription = timer2.subscribe(t => {
      this.ticks2 = t;
    });

    this.timer3 = Observable.timer(2000, 1000);
    // subscribing to a observable returns a subscription object
    this.sub = this.timer3.subscribe(t => this.tickerFunc(t));
  }

  tickerFunc(tick) {
    console.log("timer3 sub: " + this.ticks3);
    this.ticks3 = tick;
  }

  ngOnDestroy() {
    console.log("Destroy timer");
    this.subscription.unsubscribe(); //timer2
    this.sub.unsubscribe(); //timer3
  }
}
