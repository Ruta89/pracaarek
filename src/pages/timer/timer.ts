import { Component, OnInit, OnDestroy } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-timer",
  templateUrl: "timer.html"
})
export class TimerPage implements OnInit, OnDestroy {
  intervalId = 0;
  message = "";
  seconds = 10;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad TimerPage");
  }

  clearTimer() {
    // clearInterval anuluje setInterval()
    clearInterval(this.intervalId);
  }

  ngOnInit() {
    this.start();
  }
  ngOnDestroy() {
    this.clearTimer();
  }

  start() {
    this.seconds = 10;
    this.countDown();
  }
  stop() {
    this.clearTimer();
    this.message = `Wstrzymano odliczanie. Pozostało: ${this.seconds} sekund`;
  }

  private countDown() {
    this.clearTimer();
    this.intervalId = setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.message = "Koniec Czasu!!!";
      } else {
        // -- reset ********************************************
        if (this.seconds < 0) {
          this.seconds = 0;
          this.message = ``;
        }
        this.message = `Pozostało: ${this.seconds} sekund`;
      }
    }, 1000);
  }

  timer2Go() {
    this.navCtrl.push("Timer2Page");
  }
}
