import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { PracaServiceProvider } from '../../providers/praca-service/praca-service';

@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html'
})
export class UpdatePage {
  public form: any;
  items;
  public itemWll: any = '';
  public itemL1: any = '';
  public itemAuf: any = '';
  public itemAufFormat: any = '';
  public itemCzas: any = '';
  public itemIlosc: any = '';
  public itemM: any = '';
  public itemNici: any = '';
  public itemNote: string = '';
  public itemDate: any;
  public itemId: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private viewCtrl: ViewController,
    private pracaService: PracaServiceProvider
  ) {
    this.form = fb.group({
      wll: ['', Validators.required],
      l1: ['', Validators.required],
      auf: '',
      aufFormat: '',
      czas: '',
      ilosc: '',
      m: '',
      nici: '',
      note: ''
    });
    this.items = pracaService.pozycjeRef();

    let item = navParams.get('item');

    this.itemId = item.$key;
    this.itemWll = item.wll;
    this.itemL1 = item.l1;
    this.itemAuf = item.auf;
    this.itemAufFormat = item.aufFormat;
    this.itemCzas = item.czas;
    this.itemIlosc = item.ilosc;
    this.itemM = item.m;
    this.itemNici = item.nici;
    this.itemNote = item.note;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePage');
  }

  saveItem(val) {
    let wll: number = this.form.controls['wll'].value,
      l1: number = this.form.controls['l1'].value,
      auf: number = this.form.controls['auf'].value,
      aufFormat: number = this.form.controls['aufFormat'].value,
      czas: number = this.form.controls['czas'].value,
      ilosc: number = this.form.controls['ilosc'].value,
      m: number = this.form.controls['m'].value,
      nici: string = this.form.controls['nici'].value,
      note: string = this.form.controls['note'].value,
      date: any = Date.now();

    this.items.update(this.itemId, {
      wll: wll,
      l1: l1,
      auf: auf,
      aufFormat: aufFormat,
      czas: czas,
      ilosc: ilosc,
      m: m,
      nici: nici,
      note: note,
      updateDate: date
    });

    this.closeModal();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  usun() {
    this.pracaService.usunPozycje(this.itemId);
    this.closeModal();
  }
}
