import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-tabs",
  templateUrl: "tabs.html"
})
export class TabsPage {
  tab1Root = "ViewPage";
  tab2Root = "AddPage";
  tab3Root = "PracaPage";
  tab4Root = "WagaPage";
  tab5Root = "NaddatkiPage";
  tab6Root = "ViewTimePage";

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad TabsPage");
  }
}
