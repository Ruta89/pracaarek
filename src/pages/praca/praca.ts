import { Observable } from 'rxjs/Observable';
import { PracaServiceProvider } from './../../providers/praca-service/praca-service';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  AlertController,
  ActionSheetController,
  NavParams,
  ModalController,
  LoadingController,
  ToastController
} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DecimalPipe } from '@angular/common';
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database-deprecated';
import * as moment from 'moment';
class Pozycja {
  wll: number;
  l1: number;
  licznik: number;
  nici: string;
  auf: number;
  ilosc: number;
  currentPerson: any;
  jestForm: boolean = false;
  przyciskDodaj: boolean = true;
  przyciskZamknij: boolean = false;
  constructor() {}
}
@IonicPage()
@Component({
  selector: 'page-praca',
  templateUrl: 'praca.html'
})
export class PracaPage {
  formGroupDodajPozycje: FormGroup;
  date: any;
  timestamp: any;
  dateNow: any = Date.now();
  lastWeek: any;
  lastDay = Date.now() - 86400000;
  last2Day = Date.now() - 172800000;
  listaPozycji: FirebaseListObservable<any[]>;
  pozycje: Observable<any[]>;
  naddatki: Observable<any[]>;
  wll: number;
  l1: number;
  m: any;
  nici: any;
  auf: any;
  ilosc: any;
  czas: number;
  pozycja: Pozycja = new Pozycja();
  user: any;
  toggle: boolean = true;
  segmentPraca: string = 'pozycje';
  pozycjaDetail: any;
  currentUser: any;
  id: any;
  jestForm: boolean = false;
  przyciskDodaj: boolean = true;
  przyciskZamknij: boolean = false;
  resoult: any;
  mojNaddatek: any;
  naddatkiP: any;
  p: any;
  authenticated: any;
  szychta: Array<any[]>;
  robota: Array<any[][]>;
  robota2: Array<any[]> = [];
  kazdaszychta: any;
  pozycjeArr: Array<any[]>;
  value: any;

  constructor(
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public pracaService: PracaServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private modalCtrl: ModalController,
    public afd: AngularFireDatabase
  ) {
    this.createForm();
  }
  getNaddatki() {
    this.pracaService.getNaddatki();
  }
  pokaForm() {
    this.jestForm = true;
    this.przyciskDodaj = false;
    this.przyciskZamknij = true;
  }

  zamknijForm() {
    this.jestForm = false;
    this.przyciskDodaj = true;
    this.przyciskZamknij = false;
  }

  createForm() {
    this.formGroupDodajPozycje = this.formBuilder.group({
      wll: ['', Validators.required],
      l1: ['', Validators.required],
      m: [''],
      nici: [''],
      auf: [''],
      ilosc: [''],
      czas: ['']
    });
  }

  openDetail(pozycja) {
    this.navCtrl.push('DetailPozycjaPage', {
      pozycja: pozycja
    });
  }

  openDetailNaddatek(naddatek) {
    let actionSheet = this.actionSheetCtrl.create({
      title:
        'Wll: ' +
        naddatek.wll +
        ' , L1: ' +
        naddatek.l1 +
        ' - Co chcesz zrobić?',
      buttons: [
        {
          text: 'Uaktualnij naddatek',
          handler: () => {
            this.updateNaddatek(naddatek.$key, naddatek);
          }
        },
        {
          text: 'Usuń naddatek',
          role: 'destructive',
          handler: () => {
            this.pracaService.removeNaddatek(naddatek.$key);
          }
        },
        {
          text: 'Anuluj',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }

  updateNaddatek(id, naddatek) {
    let prompt = this.alertCtrl.create({
      title: 'Aktualizacja danych',
      message: 'Wprowadz poprawki ',
      inputs: [
        {
          name: 'wll',
          placeholder: 'Tonaz',
          value: naddatek.wll
        },
        {
          name: 'l1',
          placeholder: 'Dlugosc',
          value: naddatek.l1
        },
        {
          name: 'maszyna',
          placeholder: 'Maszyna',
          value: naddatek.maszyna
        },
        {
          name: 'mojNaddatek',
          placeholder: 'Naddatek',
          value: naddatek.mojNaddatek
        }
      ],
      buttons: [
        {
          text: 'Anuluj',
          handler: data => {}
        },
        {
          text: 'Zapisz',
          handler: data => {
            this.pracaService.updateNaddatek(id, data);
          }
        }
      ]
    });
    prompt.present();
  }

  zapiszPozycje(value) {
    this.pracaService.savePozycja(value).then(() => {
      this.navCtrl.setRoot('PracaPage');
    });
  }

  addNaddatek(): void {
    let prompt = this.alertCtrl.create({
      title: 'Wpisz naddatek',
      inputs: [
        {
          name: 'wll',
          type: 'number',
          placeholder: 'tonaz'
        },
        {
          name: 'l1',
          type: 'number',
          placeholder: 'dlugosc'
        },
        {
          name: 'maszyna',
          type: 'text',
          placeholder: 'maszyna'
        },
        {
          name: 'mojNaddatek',
          type: 'number',
          placeholder: 'mojNaddatek'
        }
      ],
      buttons: [
        {
          text: 'Anuluj',
          handler: data => {}
        },
        {
          text: 'Zapisz',
          handler: data => {
            this.pracaService.zapiszNaddatek(data).then(() => {});
          }
        }
      ]
    });
    prompt.present();
  }

  ionViewDidLoad() {
    console.log('Hello PracaPage');
  }

  ionViewDidEnter() {
    let loading = this.loadingCtrl.create({
      content: 'Uzyskiwanie najnowszych wpisów ...'
    });
    loading.present();
    this.pracaService.authState.subscribe(user => {
      let pozycje24 = this.pracaService.wyswietlPozycje();
      this.naddatki = this.pracaService.wyswietlNaddatki();
      if (pozycje24) {
        this.pozycje = pozycje24;
        loading.dismiss();
      } else {
        alert('nie ma pozycji');
      }
    });
  }

  setPozycjeWeek() {
    let loading = this.loadingCtrl.create({
      content: 'Uzyskiwanie  wpisów z tygodnia ...'
    });
    loading.present();
    let pozycjeWeek = this.pracaService.viewPozycjeWeek();
    if (pozycjeWeek) {
      this.pozycje = pozycjeWeek;
      loading.dismiss();
    } else {
      alert('nie ma pozycji z tygodnia');
    }
  }
  showAllPozycje() {
    let loading = this.loadingCtrl.create({
      content: 'ładowanie wpisów ...'
    });
    loading.present();
    let pozycjeWeek = this.pracaService.viewPozycjeAll();
    if (pozycjeWeek) {
      this.pozycje = pozycjeWeek;
      loading.dismiss();
    } else {
      alert('nie ma pozycji z tygodnia');
    }
  }
}
