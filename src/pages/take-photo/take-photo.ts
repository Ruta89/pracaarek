import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import { PracaServiceProvider } from './../../providers/praca-service/praca-service';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  ActionSheetController
} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-take-photo',
  templateUrl: 'take-photo.html'
})
export class TakePhotoPage {
  items: Observable<any[]>;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public pracaService: PracaServiceProvider,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TakePhotoPage');
    this.items = this.pracaService.getPhotos();
  }
  saveResults(imageData) {
    this.pracaService.savePhoto(imageData);
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 90,
      targetHeight: 500,
      targetWidth: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera
      .getPicture(options)
      .then(
        ImageData => {
          this.pracaService.savePhoto(ImageData).then(() => {
            let toastCtrl = this.toastCtrl.create({
              message: 'Zdjęcie zapisane!',
              duration: 3000
            });
            toastCtrl.present();
          });
        },
        err => {
          alert(err);
        }
      )
      .catch(err => {
        alert(err);
      });
  }

  show(photo) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opcje',
      buttons: [
        {
          text: 'Usuń',
          role: 'destructive',
          handler: () => {
            this.pracaService.deletePhoto(photo.$key);
          }
        },
        {
          text: 'Archiwizuj',
          handler: () => {
            this.pracaService.archivePhoto(photo);
          }
        },
        {
          text: 'Anuluj',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }
}
