import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPage } from './view';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    ViewPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(ViewPage),
  ],
})
export class ViewPageModule {}
