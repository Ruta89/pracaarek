import { Component, OnInit } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController
} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { PracaServiceProvider } from './../../providers/praca-service/praca-service';

@IonicPage()
@Component({
  selector: 'page-view',
  templateUrl: 'view.html'
})
export class ViewPage implements OnInit {
  items: Observable<any[]>;
  items2;
  lastDay = Date.now() - 86400000;
  last8;
  itemy;
  sumIlosc = 0;
  sumWaga = 0;
  summMinut = 0;
  averageIlosc = 0;
  averageMinut = 0;
  averageWaga = 0;
  loading;
  dateNow = Date.now();
  razemIlosc;
  razemCzas;
  razemWaga;
  allSztuk: number;
  iloscSztuk;
  times;
  times2;
  timeIlosc;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private pracaService: PracaServiceProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController
  ) {}

  ngOnInit() {}

  calculateSumIlosc(value) {
    this.sumIlosc = this.sumIlosc + parseInt(value);
  }
  calculateSumWaga(value) {
    this.sumWaga = this.sumWaga + parseInt(value);
  }
  calculateSumMinut(value) {
    this.summMinut = this.summMinut + parseInt(value);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPage');
    this.getPozycje(10).then(() => this.summary());
  }
  getPozycje(hours) {
    this.loading = this.loadingCtrl.create({
      content: 'Pobieranie ...'
    });
    this.loading.present();
    return this.pracaService
      .promisePozycje(hours)
      .then(dane => {
        this.items2 = dane;
        this.loading.dismiss();
      })
      .catch(err => {
        alert(err);
      });
  }

  summary() {
    this.items2.subscribe(data => {
      this.sumIlosc = 0;
      this.sumWaga = 0;
      this.summMinut = 0;
      data.forEach(itm => {
        this.calculateSumIlosc(itm.ilosc);
        if (itm.waga) {
          this.calculateSumWaga(itm.waga * itm.ilosc);
        }

        this.calculateSumMinut(itm.czas * itm.ilosc);
        this.getTime(itm.wll, itm.l1);
      });
    });
  }

  getTime(t, d) {
    return this.pracaService.getTimes(t, d).then(data => {
      this.times2 = data;
    });
  }

  openDetail(pozycja) {
    this.navCtrl.push('DetailPozycjaPage', {
      pozycja: pozycja
    });
  }

  toggleDetails(data) {
    this.getTime(data.wll, data.l1).then(data => (this.times = data));
    if (data.showDetails) {
      data.showDetails = false;
      data.icon = 'add-circle';
    } else {
      data.showDetails = true;
      data.icon = 'remove-circle';
    }
  }

  addWaga(data) {
    var waga = prompt('Wpisz ile wazy: ' + data.wll + 'T ' + data.l1 + 'm ');
    if (waga != null) {
      alert('Zapisuje ' + waga + ' kg / 1szt');
      this.pracaService
        .dodajWage(data, waga)
        .then(() => {
          this.pracaService.dodajWageDoPoz(data, waga);
        })
        .catch(err => console.log(err));
    } else {
      alert('Anulowałeś akcję');
    }
  }

  goAdd() {
    this.navCtrl.push('AddPage');
  }

  update(d) {
    let params = { item: d, isEditabled: true },
      modal = this.modalCtrl.create('UpdatePage', params);
    modal.present();
  }
}
