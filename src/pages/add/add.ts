import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormControlName,
  Validators
} from '@angular/forms';
import { PracaServiceProvider } from '../../providers/praca-service/praca-service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {
  @ViewChild('myInput') myInput;
  @ViewChild('myInput2') myInput2;
  @ViewChild('myInput3') myInput3;
  myForm: FormGroup;
  segment;
  min;
  max;
  czasH;
  czasM;
  formatAuf;
  licz;
  foundWaga;
  foundWaga0;
  elementWaga;
  lastAuf = null;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private pracaService: PracaServiceProvider
  ) {
    this.createForm();
    this.segment = 'step1';
    this.elementWaga = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPage');
    this.getLastAuf();

    setTimeout(() => {
      this.myInput.setFocus();
    }, 1050);
    console.log('lastAuf',this.lastAuf);
  }
  ionViewWillEnter() {}

  flipSegment() {
    if (this.segment === 'step2') {
      this.segment = 'step1';
    } else {
      this.segment = 'step2';
    }
  }

  createForm() {
    this.myForm = this.fb.group({
      wll: '',
      l1: '',
      szt: '',
      nici: '',
      auf: '',
      aufFormat: '',
      czas: ''
    });
  }

  goTo2() {
    this.flipSegment();
    setTimeout(() => {
      this.myInput2.setFocus();
    }, 550);
  }

  nextInputTab() {
    setTimeout(() => {
      this.myInput3.setFocus();
    }, 250);
  }
  nextInputEnter() {
    setTimeout(() => {
      this.myInput3.setFocus();
    }, 250);
  }

  zapiszPozycjeTab(data) {
    this.zapiszPozycjeButton(data);
    this.pracaService.addSztuk(data.ilosc);
    this.navCtrl.setRoot('TabsPage');
  }

  zapiszPozycjeButton(data): any {
    this.pracaService.savePozycjaAdd(data);
    this.pracaService.addSztuk(data.ilosc);
    this.navCtrl.setRoot('TabsPage');
  }
  getLastAuf() {
    return this.pracaService.getLastAufS().then(dane => {
      this.lastAuf = dane;
    });
  }
}
