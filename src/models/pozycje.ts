export interface Ipozycje {
  auf: number;
  aufFormat: string;
  authorEmail: string;
  authorId: string;
  czas: number;
  dateNow: number;
  fireTimeStamp: number;
  ilosc: number;
  l1: number;
  m: number;
  nici: string;
  updateDate: number;
  waga: number;
  wll: number;
}
