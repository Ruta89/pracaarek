export class Hero {
  id = 0;
  name = '';
}

export class Address {
  street = '';
  city   = '';
  state  = '';
  zip    = '';
}

export class Modele{
  segment;
  step1 = true;
  step2 = false;
}



export const States = ['CA', 'MD', 'OH', 'VA'];


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
